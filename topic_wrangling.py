import string
import jieba
import jieba.analyse
import pandas as pd
from topic_crawler import start_file

def keywords_cut(name, week):
    segments = []
    jieba.enable_paddle()
    jieba.load_userdict("dictionary/user_dict.txt")
    jieba.analyse.set_stop_words('dictionary/baidu_stopwords.txt')
    df = pd.read_csv('D:/' + name + '/topic/topic_raw_data_' + week + '.csv')

    for i in df['content']:  # 原始文本
        text = str(i)
        line = text.translate(str.maketrans('', '', string.punctuation))
        # 基于textRank算法进行关键词抽取
        keywords = jieba.analyse.textrank(line, topK=20, withWeight=False, allowPOS=('ns', 'n', 'vn', 'v', 'nw',
                                                                                     'nz', 'nr', 'f', 's', 'nt'))
        splinted_Str = ''
        for word in keywords:
            # 记录全局分词
            segments.append({'word': word, 'count': 1})
            splinted_Str += word + ' '

    # 将结果数组转为df序列
    dfSg = pd.DataFrame(segments)
    # 词频统计
    dfWord = dfSg.groupby('word')['count'].sum()
    # 导出csv
    dfWord.to_csv('D:/' + name + '/topic/keywords_textRank' + week + '.csv', encoding='utf-8')
    start_file('D:/' + name + '/topic/keywords_textRank' + week + '.csv')



