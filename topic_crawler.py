import os
import subprocess
from bs4 import BeautifulSoup
import requests
import pandas as pd

res = pd.DataFrame(columns=('name', 'time', 'content'))
pages = set()


def get_content(url, review_start, review_end):
    """main function to craw content from target url

    :param review_end: review start page parameter
    :param review_start: review end page parameter
    :param url: a target url path, where this function collects content from

    :return
    """
    global res
    try:
        html = requests.get(url)
        bs = BeautifulSoup(html.text, 'html.parser')
        userName = bs.find("a", class_="user-name").string
        print("用户名：" + userName)

        ul = bs.find("ul", class_="topic-info")
        li = [td.string for td in ul.find('span')]
        print("发帖时间: " + str(li[0]))

        content = bs.find("div", class_="bbcode-body-v2").text
        print("内容：" + content.replace("\n", ""))
        res = res.append({'name': userName, 'time': str(li[0]), 'content': content.strip().replace("\n", "")},
                         ignore_index=True)
        try:
            for j in range(review_start, review_end + 1):
                get_review(url + '?order=desc&sort=position&show_all=0&page=' + str(
                    j) + '#postsList')
        except ConnectionRefusedError:
            print('有内鬼，终止交易')

    except AttributeError:
        print('页面有错误！继续获取爬取新内容')
    return res


def geturl(url, review_start, review_end):
    """get all the topic content url path from current page then add all url path into page set

    :param review_end: transmit parameter to get_review()
    :param review_start: transmit parameter to get_review()
    :param url: a url path, will collect all the url path from this url

    """
    global pages
    global res
    html = requests.get(url)
    bs = BeautifulSoup(html.text, 'html.parser')
    content = bs.find_all("a", class_="taptap-btn-link")
    try:
        print("开始爬取新页面的帖子和评论")

    except AttributeError:
        print('页面有错误！请继续')

    for link in content:
        if 'href' in link.attrs:
            if link.attrs['href'] not in pages:
                newPage = link.attrs['href']
                print('-' * 20)
                print(newPage)
                pages.add(newPage)
                res.append(get_content(newPage, review_start, review_end))
    print('-' * 20)
    print("爬取当前页面内容结束")


def get_review(url):
    """get all the review content from current topic page
    the review content is under the topic content

    :param url: a url path, will collect all the url path from this url

    """
    global res
    try:
        html = requests.get(url)
        bs = BeautifulSoup(html.text, 'html.parser')
        commentList = bs.find_all("div", class_='posts-item-text topic-posts-item-text')
        for item in commentList:
            name = item.find('a')
            time = item.find('li', class_='post-dynamic-time')
            content = item.find('div', class_='item-text-body bbcode-body bbcode-body-v2 js-open-bbcode-image')

            res = res.append({
                'name': name.text.strip().replace("\n", ""), 'time': time.text.strip().replace("\n", ""),
                'content': content.text.strip().replace("\n", "")
            }, ignore_index=True)

    except AttributeError:
        print('页面有错误！继续获取爬取新内容')


def start_file(filename):
    print('正在打开文档...')
    try:
        os.startfile(filename)
    except OSError:
        subprocess.Popen(['xdg-open', filename])


def crawler_run(start, end, gameCode, review_start, review_end, path):
    global pages
    global res
    try:
        res = pd.read_csv(path, encoding='utf_8_sig', index_col=0)
    except PermissionError:
        print("Error: 文件被其他占用，读取失败，请关闭其他文件")
    except IOError:
        print("Error: 没有找到文件, 创建文件并开始爬取...")
    else:
        print("文件已经找到，开始写入")

    try:
        for i in range(start, end + 1):
            geturl(
                'https://www.taptap.com/app/' + str(gameCode) + '/topic?type=feed&sort=published&page=' + str(i),
                review_start, review_end)
    except ConnectionRefusedError:
        print('有内鬼，终止交易')
    res.to_csv(path, encoding='utf_8_sig', index=False)
