import pandas as pd
from snownlp import SnowNLP
import requests
import json
import time


def get_sentiments_snow(text):
    s = SnowNLP(str(text))
    return s.sentiments


def snowNLP(path):
    df = pd.read_csv(path)
    print('开始进行情感分析...')
    df['snownlp_sentiment'] = df.content.apply(get_sentiments_snow)
    df.to_csv(path, encoding='utf-8', index=False)



def get_sentiments_baidu(text):
    # client_id 为官网获取的AK， client_secret 为官网获取的SK
    host = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=QkOOcEinG1GKkxC2SLRWYIo7' \
           '&client_secret=P2f34YvzmfMMdymAqbGcMl0cUqNQb6vM'
    response = requests.get(host)
    if response:
        access_token = response.json()['access_token']
        url = 'https://aip.baidubce.com/rpc/2.0/nlp/v1/sentiment_classify?charset=UTF-8&access_token=' + access_token

        # 按照要求设置请求头
        headers = {'content-type': 'application/json'}

        # 用requests工具包发送请求
        response = requests.post(url,
                                 data=json.dumps({'text': str(text)}),  # st变量写在这里
                                 headers=headers)
        time.sleep(1)
        if response:
            return response.json()['items'][0]['positive_prob']
        else:
            return 0

def baiduNLP():
    # 目前很不好用，不推荐
    file = 'd:/ulala/topic/topic_raw_data_10.16-10.30.csv'
    df = pd.read_csv(file)
    df['baidu_sentiment'] = df.content.apply(get_sentiments_baidu)
    df.to_csv(file, encoding='utf-8')



