import topic_wrangling
import topic_crawler
import topic_emotion
import hanlp
import time

'''setting crawler parameters
:param week: target file name
:param start: start page
:param end: end page
:param review_start: review start page
:param review_end: review end page
'''

week = '10.16-10.31'
start = 20
end = 90

review_start = 1
review_end = 1

''' setting target app parameters setting
:param name: app name
:param gameCode: app code from taptap
:param for example: https://www.taptap.com/app/133684 ulala's app code is 133684
'''

name = 'ulala'
gameCode = 133684

'''try to read existing file from target path
if failed print error
if success print succeed
'''

if __name__ == '__main__':
    start_time = time.time()
    path = 'D:/' + name + '/topic/topic_raw_data_' + week + '.csv'

    time.sleep(5)
    topic_crawler.crawler_run(start=start, end=end, review_start=review_start, review_end=review_end, path=path,
                              gameCode=gameCode)

    time.sleep(5)
    topic_wrangling.keywords_cut(name, week)

    time.sleep(5)
    topic_emotion.snowNLP(path)

    time.sleep(5)
    hanlp.hanLP_run(path)

    time.sleep(5)
    topic_crawler.start_file(path)

    end_time = time.time()
    print(end_time - start_time)