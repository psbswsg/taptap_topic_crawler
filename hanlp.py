from pyhanlp import *
import pandas as pd


def sentence_extraction(text):
    # 关键词提取
    print(HanLP.extractKeyword(text, 10))
    # 短语提取
    print(HanLP.extractPhrase(text, 10))
    # 自动摘要
    result = HanLP.extractSummary(text, 10)
    for summary in result:
        print(summary)


def predict(classifier, text):
    # print("《%s》 情感极性是 【%s】" % (text, classifier.classify(text)))
    result = classifier.classify(text)
    return result


def hanLP_run(path):
    NaiveBayesClassifier = JClass('com.hankcs.hanlp.classification.classifiers.NaiveBayesClassifier')
    # 中文情感挖掘语料-ChnSentiCorp 谭松波
    chn_senti_corp = 'sentimentAnalysis/ChnSentiCorp情感分析酒店评论'
    df = pd.read_csv(path)
    classifier = NaiveBayesClassifier()
    #  创建分类器，更高级的功能请参考IClassifier的接口定义
    classifier.train(chn_senti_corp)
    # #  训练后的模型支持持久化，下次就不必训练了
    resultList = []
    for i in range(len(df)):
        resultList.append(predict(classifier, str(df.content.iloc[i])))
    df['hanlp'] = resultList
    df.to_csv(path, index=False)
    print('情感分析结束...')


